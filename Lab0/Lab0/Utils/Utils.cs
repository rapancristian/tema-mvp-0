﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;


namespace Lab0.Util
{
    public class Utils
    {
        public  static string getHelloMessage(string message)
        {
            return "Hello " + message;
        }

        public static bool IsOnlyNumbers(string text)
        {
            string[] numbers = text.Split(' ');
            bool isNumeric;
            for (int i = 0; i < text.Split(' ').Length; i++)
            {
                isNumeric = int.TryParse(numbers[i], out int result);
                if (result == 0)
                {
                    return false;
                }
            }
            return true;
        }

        public static float GetAverageList(List<int> numbers)
        {
            int sum = 0;
            foreach (int number in numbers)
            {
                sum += number;
            }
            return (float)sum / numbers.Count();
        }


    }
}


    /*
    namespace Lab0.Util
{
    public sealed class Utils
    {
        private static readonly Utils instance = new Utils();

        // Explicit static constructor to tell C# compiler
        // not to mark type as beforefieldinit
        static Utils()
        {
        }

        private Utils()
        {
        }

        public static Utils Instance
        {
            get
            {
                return instance;
            }
        }

        


    }
}
*/
