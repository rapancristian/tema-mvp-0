﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows;
using System.Windows.Controls;
using System.Windows.Data;
using System.Windows.Documents;
using System.Windows.Input;
using System.Windows.Media;
using System.Windows.Media.Imaging;
using System.Windows.Navigation;
using System.Windows.Shapes;
using Lab0.Util;

namespace Lab0
{
    /// <summary>
    /// Interaction logic for MainWindow.xaml
    /// </summary>
    public partial class MainWindow : Window
    {
        public MainWindow()
        {
            InitializeComponent();
        }

        public void ShowAverageOld(string text)
        {
            Double avg = text.Split(' ').Select(double.Parse).Average();
            lblResult.Content = "Media elementelor este: " + avg;
        }

        public void ShowResult(string message)
        {
            lblResult.Content = message;
            //show result on fb
        }

        public void CheckIfInputIsPalindrom(string text)
        {
            Cuvant cuvant = new Cuvant(text);
            if (!cuvant.IsPalindrome())
            {
                lblResult.Content = "Cuvantul " + txtInput.Text + " nu este palindrom";
                return;
            }
            lblResult.Content = "Cuvantul " + txtInput.Text + " este palindrom";
        }

        public string GetCheckedRadioName(WrapPanel aWrapPanel)
        {
            foreach (RadioButton rb in aWrapPanel.Children.OfType<RadioButton>())
            {
                if (rb.IsChecked == true)
                {
                    return rb.Name;
                }
            }
            return null;

        }

        public void ClearWindow()
        {
            lblInfo.Content = null;
            lblResult.Content = null;
        }

        public void HandleUserInput(string text)
        {
            ClearWindow();
            string option = GetCheckedRadioName(wpMainPanel);
            if (option == null)
            {
                lblInfo.Content = "Nici un buton selectat";
                return;
            }

            switch (option)
            {
                case "rbHello":
                    HandleRbHello(text);
                    break;
                case "rbAverage":
                    HandleRbAverage(text);
                    break;
                case "rbPalindrom":
                    HandleRbPalindrom(text);
                    break;
            }
        }

        public bool IsTxtInputEmpty(string text)
        {
            //if (!(text.Length > 0))
            if(string.IsNullOrEmpty(text))
            {
                lblInfo.Content = "Introduceti un string cu lungimea > 0";
                return false;
            }
            return true;
        }

        public void HandleRbHello(string text)
        {
            if (!IsTxtInputEmpty(text))
                return;
            lblResult.Content = Utils.getHelloMessage(text);
        }
       
        public bool IsAverageInputOk(string text)
        {
            if (string.IsNullOrEmpty(text))
            {
                lblInfo.Content = "Introduceti 2 sau mai multe numere";
                return false;
            }

            if (!Utils.IsOnlyNumbers(text))
            {
                lblInfo.Content = "Introduceti doar numere";
                return false;
            }
            List<int> SplittedNumbers = text.SplitIntoIntegers();
            if (!(SplittedNumbers.Count >= 2))
            {
                lblInfo.Content = "Introduceti 2 sau mai multe numere";
                return false;
            }
            return true;

        }

        public void HandleRbAverage(string text)
        {
            if (!IsAverageInputOk(text))
                return;
            float avg = Utils.GetAverageList(text.SplitIntoIntegers());
            ShowResult("Media elementelor este: " + avg.ToString());         
        }

        public void HandleRbPalindrom(string text)
        {
            if (!IsTxtInputEmpty(text))
                return;
            CheckIfInputIsPalindrom(text);
        }

        private void BtnDo_Click(object sender, RoutedEventArgs e)
        {
            HandleUserInput(txtInput.Text);
        }

        private void RbHello_Checked(object sender, RoutedEventArgs e)
        {
            lblInfo.Content = "Introduceti un string cu lungimea > 0";
        }

        private void RbAverage_Checked(object sender, RoutedEventArgs e)
        {
            lblInfo.Content = "Introduceti doua sau mai multe numere";
        }

        private void RbPalindrom_Checked(object sender, RoutedEventArgs e)
        {
            lblInfo.Content = "Introduceti un string cu lungimea > 0";
        }

        private void TxtInput_TextChanged(object sender, TextChangedEventArgs e)
        {

        }
    }
}
