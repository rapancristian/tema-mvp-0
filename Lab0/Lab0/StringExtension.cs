﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Lab0
{
    public static class StringExtension
    {
        public static bool IsPalindrome(this string str)
        {
            return str.SequenceEqual(str.Reverse());
        }

        public static List<int> SplitIntoIntegers(this string text)
        {
            List<int> myNumbers = new List<int>();
            int i;
            foreach (string str in text.Split(' '))
            {
                if (!int.TryParse(str, out i))
                {
                    return null;
                }
                myNumbers.Add(int.Parse(str));
            }
            return myNumbers;
        }
    }
}
